extends Sprite2D

var pos := Vector2i(0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func reset_position():
	var wall = true
	var p
	while wall:
		p = Vector2i(randi_range(1, 31), randi_range(1, 31))
		wall = %TileMap.is_wall(p)
	pos = p
	position = %TileMap.to_global(%TileMap.map_to_local(pos))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_move_timer_timeout():
	var dir = randi_range(0, 3)
	if dir == 0 and not %TileMap.is_wall(pos + Vector2i(-1, 0)):
		pos += Vector2i(-1, 0)
	if dir == 1 and not %TileMap.is_wall(pos + Vector2i(1, 0)):
		pos += Vector2i(1, 0)
	if dir == 2 and not %TileMap.is_wall(pos + Vector2i(0, -1)):
		pos += Vector2i(0, -1)
	if dir == 3 and not %TileMap.is_wall(pos + Vector2i(0, 1)):
		pos += Vector2i(0, 1)
	position = %TileMap.to_global(%TileMap.map_to_local(pos))
	_on_pc_moved(pos)


func _on_ready():
	pass

func _on_tile_map_ready():
	reset_position()


func _on_pc_moved(p: Vector2i):
	if %PC.collides_with(pos):
		get_parent().pause()

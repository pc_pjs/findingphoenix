extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_tile_map_victory():
	get_tree().quit()

func pause():
	%AnimationTimer.stop()
	%MoveTimer.stop()
	%Entry.visible = true
	pass
func play():
	%Entry.visible = false
	pass
func restart():
	%Entry.visible = false
	pass

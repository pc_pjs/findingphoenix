extends Sprite2D

signal moved(Vector2i)

var pos = Vector2i(0, 16)
var hidden_dir = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func collides_with(pos: Vector2i) -> bool:
	return self.pos == pos


func _input(event):
	var moved = -1
	if event.is_action("Right") and pos.x < 31 and not event.is_action_released("Right"):
		if not %TileMap.is_wall(Vector2i(pos.x+1, pos.y)):
			pos.x += 1
			moved = 0
	elif event.is_action("Left") and pos.x > 0 and not event.is_action_released("Left"):
		if not %TileMap.is_wall(Vector2i(pos.x-1, pos.y)):
			pos.x -= 1
			moved = 2
	elif event.is_action("Down") and pos.y < 31 and not event.is_action_released("Down"):
		if %TileMap.get_cell_atlas_coords(0, Vector2i(pos.x, pos.y+1)).y >= 2:
			pos.y += 1
			moved = 3
	elif event.is_action("Up") and pos.y > 0 and not event.is_action_released("Up"):
		if %TileMap.get_cell_atlas_coords(0, Vector2i(pos.x, pos.y-1)).y >= 2:
			pos.y -= 1
			moved = 1
	if moved > -1:
		self.position = %TileMap.to_global(%TileMap.map_to_local(pos))
		emit_signal("moved", pos)
	

func update_hidden(dir: int):
	if dir == hidden_dir:
		pass

func _on_tile_map_ready():
	pass

extends TileMap

signal victory()

const EAST = Vector2i(1, 0)
const NORTH = Vector2i(0, -1)
const SOUTH = Vector2i(0, 1)
const WEST = Vector2i(-1, 0)
enum TileType {HOME, OPPOSITE, BEHIND, ABOVE}

var phoenix : Vector2i

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var map_state = []
	for i in 32:
		var line = []
		for j in 32:
			self.set_cell(0, Vector2i(i, j), 0, Vector2i(randi_range(0, 15), randi_range(0, 1)))
			if i == 0 or j == 0 or i == 31 or j == 31:
				line.append([0, true])
			else:
				line.append([-1, false])
		map_state.append(line)
	
	var forward := 60
	var straighten := 30
	var backtracking_threshold := 100
	var back = 0
	var overwrite_limit := 20
	# Tile 0
	self.set_cell(0, Vector2i(0, 16), 0, Vector2i(6+randi_range(0, 5), 3))
	var tiles = []
	var remaining = 30*30
	var old_tile = Vector2i(0, 16)
	# Tile 1
	var turn = 1
	var current_tile = Vector2i(1, 16)
	self.set_cell(0, current_tile, 0, floor_sprite())
	map_state[current_tile.x][current_tile.y] = [turn, true]
	surround(current_tile, map_state, turn)
	tiles.append(current_tile)
	
	
	while remaining > 0 and turn < 30*30*3:
		# DIRECTION
		var new_tile_found = false
		var backwards = false
		var direction = Vector2i(0, 0)
		if free_neighbours(current_tile, map_state, turn) <= 1: # backwards counts
			# higher chances of backtracking
			backtracking_threshold = 40
		while not new_tile_found:
			backwards = false
			var roll = randi_range(0, 100)
			# same dir as previous
			direction = current_tile - old_tile
			if roll >= backtracking_threshold:
				# inverse dir
				backwards = true
				direction = old_tile - current_tile
			else:
				roll = randi_range(0, 100)
				if backtracking_threshold < 100:
					backtracking_threshold = 100
				var bias = 0

				if tile_type(current_tile) == TileType.ABOVE or tile_type(current_tile) == TileType.BEHIND:
					bias = straighten
					if ((old_tile == EAST or old_tile == WEST) and tile_type(current_tile) == TileType.ABOVE) or ((old_tile == NORTH or old_tile == SOUTH) and tile_type(current_tile) == TileType.BEHIND):
						bias = -bias
				
				if roll > (forward+bias):
					if randi_range(0, 1) == 0:
						direction = old_tile - current_tile
					else:
						direction = current_tile - old_tile
					# left / right directions
					direction = Vector2i(direction.y, direction.x)
			if backwards or (is_in(current_tile + direction) and ((not map_state[current_tile.x+direction.x][current_tile.y+direction.y][1] and randi_range(0, 5) == 0) or map_state[current_tile.x+direction.x][current_tile.y+direction.y][0] > turn - overwrite_limit)):
				new_tile_found = true
		
		# MOVE
		turn += 1
		if backwards and tiles.size()-2-back*2-1 >= 0:
			current_tile = tiles[tiles.size()-2-back*2]
			old_tile = tiles[tiles.size()-2-back*2-1]
			back += 1
			tiles.append(current_tile)
			#self.set_cell(0, current_tile, 0, Vector2i(6+randi_range(0, 5), 3))
			#print(turn)
		else:
			back = 0
			old_tile = current_tile
			current_tile = current_tile + direction
			self.set_cell(0, current_tile, 0, floor_sprite())
			map_state[current_tile.x][current_tile.y] = [turn, true]
			remaining -= 1
			surround(current_tile, map_state, turn)
			tiles.append(current_tile)
	self.set_cell(0, current_tile, 0, Vector2i(14, 3))
	phoenix = current_tile


func is_in(pos: Vector2i):
	if 0 < pos.x and pos.x < 31 and 0 < pos.y and pos.y < 31:
		return pos
	return false

func tile_type(pos: Vector2i) -> TileType:
	if pos.x % 2 == 1 and pos.y % 2 == 0:
		return TileType.HOME
	elif pos.x % 2 == 0 and pos.y % 2 == 1:
		return TileType.OPPOSITE
	elif pos.x % 2 == 0 and pos.y % 2 == 0:
		return TileType.BEHIND
	else:
		return TileType.ABOVE

# Get floor position
func floor_sprite():
	return Vector2i(6+randi_range(0, 5), 2)

#
func surround(tile: Vector2i, map_tile, turn):
	if tile.x > 0 and map_tile[tile.x-1][tile.y][0] == -1:
		self.set_cell(0, Vector2i(tile.x-1, tile.y), 0, Vector2i(randi_range(0, 15), randi_range(0, 1)))
		map_tile[tile.x-1][tile.y][0] = turn
	if tile.x < 31 and map_tile[tile.x+1][tile.y][0] == -1:
		self.set_cell(0, Vector2i(tile.x+1, tile.y), 0, Vector2i(randi_range(0, 15), randi_range(0, 1)))
		map_tile[tile.x+1][tile.y][0] = turn
	if tile.y > 0 and map_tile[tile.x][tile.y-1][0] == -1:
		self.set_cell(0, Vector2i(tile.x, tile.y-1), 0, Vector2i(randi_range(0, 15), randi_range(0, 1)))
		map_tile[tile.x][tile.y-1][0] = turn
	if tile.y < 31 and map_tile[tile.x][tile.y+1][0] == -1:
		self.set_cell(0, Vector2i(tile.x, tile.y+1), 0, Vector2i(randi_range(0, 15), randi_range(0, 1)))
		map_tile[tile.x][tile.y+1][0] = turn

func free_neighbours(tile: Vector2i, map_tile, turn) -> int:
	var neighbours = 0
	if tile.x > 0 and (not map_tile[tile.x-1][tile.y][1] or map_tile[tile.x-1][tile.y][0] >= turn - 6):
		neighbours += 1
	if tile.x < 31 and (not map_tile[tile.x+1][tile.y][1] or map_tile[tile.x+1][tile.y][0] >= turn - 6):
		neighbours += 1
	if tile.y > 0 and (not map_tile[tile.x][tile.y-1][1] or map_tile[tile.x][tile.y-1][0] >= turn - 6):
		neighbours += 1
	if tile.y < 31 and (not map_tile[tile.x][tile.y+1][1] or map_tile[tile.x][tile.y+1][0] >= turn - 6):
		neighbours += 1
	return neighbours


func is_wall(tile: Vector2i) -> bool:
	return %TileMap.get_cell_atlas_coords(0, tile).y < 2


func _on_pc_moved(pc: Vector2i):
	if pc == phoenix:
		emit_signal("victory")

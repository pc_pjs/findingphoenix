extends TileMap

var prev := Vector2i(-1, 16)
var current := Vector2i(0, 16)
var propagations : Array

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	for i in 30:
		for j in 30:
			self.set_cell(0, Vector2i(i+1, j+1),0, Vector2i(0, 0), randi_range(0, 3))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_pc_moved(pos: Vector2i):
	prev = current
	current = pos
	self.erase_cell(0, pos)
	surround(pos)
	propagate(pos, current-prev, true)

func surround(tile: Vector2i):
	if tile.x > 0:
		self.erase_cell(0, Vector2i(tile.x-1, tile.y))
	if tile.x < 31:
		self.erase_cell(0, Vector2i(tile.x+1, tile.y))
	if tile.y > 0:
		self.erase_cell(0, Vector2i(tile.x, tile.y-1))
	if tile.y < 31:
		self.erase_cell(0, Vector2i(tile.x, tile.y+1))

func propagate(pos: Vector2i, dir: Vector2i, start: bool) -> bool:
	pos += dir
	if not %TileMap.is_wall(pos):
		self.erase_cell(0, pos)
		surround(pos)
		if start:
			propagations.append([pos, dir])
			if %AnimationTimer.is_stopped():
				%AnimationTimer.start()
		return true
	else:
		return false


func _on_timer_timeout():
	var to_delete = []
	for i in propagations.size():
		if propagate(propagations[i][0], propagations[i][1], false):
			propagations[i][0] += propagations[i][1]
		else:
			to_delete.append(i)
		i += 1
	to_delete.reverse()
	for idx in to_delete:
		propagations.remove_at(idx)
	if propagations.size() == 0:
		%AnimationTimer.stop()

extends TileMap


# Called when the node enters the scene tree for the first time.
func _ready():
	for i in 32:
		for j in 32:
			self.set_cell(0, Vector2i(i, j),0, Vector2i(0, 3), randi_range(0, 2))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
